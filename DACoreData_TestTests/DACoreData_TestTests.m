//
//  DACoreData_TestTests.m
//  DACoreData_TestTests
//
//  Created by David Ansermot on 09.12.13.
//  Copyright (c) 2013 mArmApp. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "DACoreData.h"

@interface DACoreData_TestTests : XCTestCase

@end

@implementation DACoreData_TestTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

/**
 * Testing [DACoreData defaultContext]
 *
 * Part 1 : Test not nil
 * Part 2 : Test class
 */
- (void)testMOC1 {
    
    NSManagedObjectContext *moc = [DACoreData defaultContext];
    
    XCTAssertTrue(moc != nil, @"Part 1 failed");
    XCTAssertTrue([moc class] == [NSManagedObjectContext class], @"Part 2 failed");
}



/**
 * Testing [DACoreData sharedCoreData].managedObjectContext
 *
 * Part 1 : Test not nil
 * Part 2 : Test class
 */
- (void)testMOC2 {
    
    NSManagedObjectContext *moc = [DACoreData sharedCoreData].managedObjectContext;
    
    XCTAssertTrue(moc != nil, @"Part 1 failed");
    XCTAssertTrue([moc class] == [NSManagedObjectContext class], @"Part 2 failed");
}



/**
 * Testing [DACoreData sharedCoreData].managedObjectModel
 *
 * Part 1 : Test not nil
 * Part 2 : Test class
 */
- (void)testMODEL {
    
    NSManagedObjectModel *mod = [DACoreData sharedCoreData].managedObjectModel;
    
    XCTAssertTrue(mod != nil, @"Part 1 failed");
    XCTAssertTrue([mod class] == [NSManagedObjectModel class], @"Part 2 failed");
}


@end
