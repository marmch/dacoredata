//
//  Guy.h
//  DACoreData_Test
//
//  Created by David Ansermot on 10.12.13.
//  Copyright (c) 2013 mArmApp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Thinking;

@interface Guy : NSManagedObject

@property (nonatomic, retain) NSString * firstname;
@property (nonatomic, retain) NSString * lastname;
@property (nonatomic, retain) NSSet *thinkings;
@end

@interface Guy (CoreDataGeneratedAccessors)

- (void)addThinkingsObject:(Thinking *)value;
- (void)removeThinkingsObject:(Thinking *)value;
- (void)addThinkings:(NSSet *)values;
- (void)removeThinkings:(NSSet *)values;

@end
