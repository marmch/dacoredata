//
//  Thinking.h
//  DACoreData_Test
//
//  Created by David Ansermot on 10.12.13.
//  Copyright (c) 2013 mArmApp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Thinking;

@interface Thinking : NSManagedObject

@property (nonatomic, retain) NSString * content;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSSet *guy;
@end

@interface Thinking (CoreDataGeneratedAccessors)

- (void)addGuyObject:(Thinking *)value;
- (void)removeGuyObject:(Thinking *)value;
- (void)addGuy:(NSSet *)values;
- (void)removeGuy:(NSSet *)values;

@end
