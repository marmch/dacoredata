//
//  Guy.m
//  DACoreData_Test
//
//  Created by David Ansermot on 10.12.13.
//  Copyright (c) 2013 mArmApp. All rights reserved.
//

#import "Guy.h"
#import "Thinking.h"


@implementation Guy

@dynamic firstname;
@dynamic lastname;
@dynamic thinkings;

@end
