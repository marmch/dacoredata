//
//  NameViewController.m
//  DACoreData_Test
//
//  Created by David Ansermot on 10.12.13.
//  Copyright (c) 2013 mArmApp. All rights reserved.
//

#import "NameViewController.h"

@interface NameViewController ()

@end

@implementation NameViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneButton_clicked:(UIBarButtonItem *)sender {
    
    NSString *first = self.firstnameTF.text;
    NSString *last = self.lastnameTF.text;
    
    if (![first isEqualToString:@""] && ![last isEqualToString:@""]) {
        [self.delegate didSavedFirstname:first andLastname:last];
    }
    
}

@end
