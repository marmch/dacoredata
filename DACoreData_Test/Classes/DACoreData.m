//
//  DACoreData.m
//  DACoreData_Test
//
//  Created by David Ansermot on 09.12.13.
//  Copyright (c) 2013 mArmApp. All rights reserved.
//

#import "DACoreData.h"

// Key for models
static NSString *kManagedModelName = @"DADataModel";


#pragma mark - Class private methods

@interface DACoreData (PrivateMethods)

+ (void)clear;
+ (void)throwMOCsaveException;
+ (void)throwCoordinatorCreateException;

- (void)observeManagedContext:(NSManagedObjectContext *)context;
- (void)stopObservingManagedContext:(NSManagedObjectContext *)context;
- (void)mergeChangesFromNotification:(NSNotification *)notify;

- (void)managedObjectContextSaved:(NSNotification *)notify;

// To fake the readonly on public properties
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@end










#pragma mark - Class implementation

@implementation DACoreData

// Handle the singleton instance of the class
static DACoreData __strong *__instance = nil;

@synthesize managedObjectContext = _managedContext;
@synthesize managedObjectModel = _managedModel;
@synthesize persistentStoreCoordinator = _persistentCoordinator;











#pragma mark - Class initialization

/**
 * Default init method
 *
 * @param void
 * @return void
 * @access public
 */
- (id)init {
    if (self = [super init]) { }
    return self;
}










#pragma mark - Singleton design pattern methods

/**
 * Return the singleton object of the class
 *
 * @param void
 * @return DACoreData*
 * @access public
 * @static
 */
+ (DACoreData *)sharedCoreData {
    static dispatch_once_t once;
    
    dispatch_once(&once, ^{
        __instance = [[DACoreData alloc] init];
    });
    return __instance;
}



#ifdef _DEBUG_
/**
 * DEBUG : Logs managed object context saved notification
 *
 * @params NSNotification*
 * @return void
 * @access public
 */
- (void)managedObjectContextSaved:(NSNotification *)notify {
    NSLog(@"Managed Object Context saved with success.");
}
#endif



/**
 * Remove objects in DACoreData
 *
 * @param void
 * @return void
 * @access private
 * @static
 */
+ (void)clear {
    [NSObject cancelPreviousPerformRequestsWithTarget:[DACoreData class]];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [DACoreData sharedCoreData].managedObjectContext = nil;
    [DACoreData sharedCoreData].managedObjectModel = nil;
    [DACoreData sharedCoreData].persistentStoreCoordinator = nil;
}



/**
 * Raise moc saving exception
 *
 * @param void
 * @return void
 * @access private
 * @static
 */
+ (void)throwMOCsaveException {
    NSException *exception = [NSException exceptionWithName:@"CoreData exception"
                                                     reason:@"Unable to save the default context."
                                                   userInfo:nil];
    @throw exception;
}



/**
 * Raise coordinator create exception
 *
 * @param void
 * @return void
 * @access private
 * @static
 */
+ (void)throwCoordinatorCreateException {
    NSException *exception = [NSException exceptionWithName:@"CoreData exception"
                                                     reason:@"Unable to create persistent store coordinator."
                                                   userInfo:nil];
    @throw exception;
}










#pragma mark - Managed context related methods

/**
 * Returns the default context
 *
 * @param void
 * @return NSManagedObjectContext*
 * @access public
 * @static
 */
+ (NSManagedObjectContext *)defaultContext {
    return [DACoreData sharedCoreData].managedObjectContext;
}



/**
 * Saves the default context
 *
 * @param void
 * @return void
 * @access public
 * @static
 */
+ (BOOL)saveDefaultContext {
    [NSObject cancelPreviousPerformRequestsWithTarget:[DACoreData class] selector:@selector(saveDefaultContext) object:nil];
    
    NSError *error = nil;
    [[DACoreData defaultContext] save:&error];
    
    if (error != nil) {
        return false;
        //[self throwMOCsaveException];
    } else {
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationDACoreDataHasSaved object:nil];
        return true;
    }
}



/**
 * Set have to save the context
 *
 * @param void
 * @return void
 * @access public
 * @static
 */
+ (void)haveToSaveDefaultContext {
    [NSObject cancelPreviousPerformRequestsWithTarget:[DACoreData class] selector:@selector(saveDefaultContext) object:nil];
    [[DACoreData class] performSelector:@selector(saveDefaultContext) withObject:nil afterDelay:0.2];
}



/**
 * Returns documents directory path
 *
 * @param void
 * @return void
 * @access public
 * @static
 */
- (NSString *) applicationDocumentsDirectory {
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}



/**
 * Set Data Model filename
 *
 * @param NSString*
 * @return void
 * @access public
 * @static
 */
+ (void)setModelName:(NSString *)modelName {
    kManagedModelName = modelName;
}











#pragma mark - Tasks related methods

/**
 * Run a task block synchronous
 *
 * @param @block block
 * @return void
 * @access public
 * @static
 */
+ (void)runTaskBlock:(void (^)(NSManagedObjectContext *))block {
    NSManagedObjectContext *mainContext = [DACoreData defaultContext];
    NSManagedObjectContext *threadContext = nil;
    
    // Create the second context for the thread
    if (![NSThread isMainThread]) {
        threadContext = [[NSManagedObjectContext alloc] init];
        threadContext.persistentStoreCoordinator = mainContext.persistentStoreCoordinator;
        
        [mainContext setMergePolicy:NSMergeByPropertyObjectTrumpMergePolicy];
        [threadContext setMergePolicy:NSOverwriteMergePolicy];
        
        [[DACoreData sharedCoreData] observeManagedContext:threadContext];
    }
    
    block(threadContext);
    
    // Try to save the context
    NSError *error;
    if (![threadContext save:&error]) {
        [self throwMOCsaveException];
    }
    
}



/**
 * Run a task block asynchronous
 *
 * @param @block block
 * @return void
 * @access public
 * @static
 */
+ (void)runTaskBlockInBackground:(void (^)(NSManagedObjectContext *))block {
    [[self class] runTaskBlockInBackground:block completion:nil priority:-1];
}



/**
 * Run a task block synchronous with a callback
 *
 * @param @block block
 * @param @block callback
 * @return void
 * @access public
 * @static
 */
+ (void)runTaskBlockInBackground:(void (^)(NSManagedObjectContext *))block completion:(void (^)(void))callback {
    [[self class] runTaskBlockInBackground:block completion:callback priority:-1];
}



/**
 * Run a task block asynchronous
 *
 * @param @block block
 * @param @block callback
 * @param NSInteger priority (must be between 0 and 3, forced to 1 if not in this range)
 * @return void
 * @access public
 * @static
 */
+ (void)runTaskBlockInBackground:(void (^)(NSManagedObjectContext *))block completion:(void (^)(void))callback priority:(NSInteger)priority {
    
    // Sets the final task priority
    NSInteger realPriority = (priority > -1 && priority < 4) ? priority : DISPATCH_QUEUE_PRIORITY_DEFAULT;
    
    // Run async
    dispatch_queue_t asyncQueue = dispatch_get_global_queue(realPriority, 0);
    dispatch_async(asyncQueue, ^{
        [[self class] runTaskBlock:block];
        
        if (callback != nil) {
            dispatch_async(dispatch_get_main_queue(), callback);
        }
    });
}










#pragma mark - Override method for accessors

/**
 * Create / return managed object context
 *
 * @param void
 * @return NSManagedObjectContext*
 * @access public
 */
- (NSManagedObjectContext *)managedObjectContext {
    
    // Return context if exists
    if (_managedContext != nil) {
        return _managedContext;
    }
    
    // Create the context
    NSPersistentStoreCoordinator *storeCoordinator = self.persistentStoreCoordinator;
    if (storeCoordinator != nil) {
        _managedContext = [[NSManagedObjectContext alloc] init];
        [_managedContext setPersistentStoreCoordinator:storeCoordinator];
#ifdef _DEBUG_
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(managedObjectContextSaved:) name:NSManagedObjectContextDidSaveNotification object:_managedContext];
#endif
    }
    return _managedContext;
}



/**
 * Create / return managed object model
 *
 * @param void
 * @return NSManagedObjectModel
 * @access public
 */
- (NSManagedObjectModel *)managedObjectModel {
    
    // Return model if exists
    if (_managedModel != nil) {
        return _managedModel;
    }
    
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:kManagedModelName withExtension:@"momd"];
    _managedModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    
    return _managedModel;
    
}



/**
 * Create / return persistent store coordinatore
 *
 * @param void
 * @return NSPersistentStoreCoordinator*
 * @access public
 */
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    
    // Return coordinator if exists
    if (_persistentCoordinator != nil) {
        return _persistentCoordinator;
    }
    
    // Create coordinator
    NSString *coordinatorPath = [NSString stringWithFormat:@"%@.%@", kManagedModelName, @"sqlite"];
    NSURL *appDocumentsDir = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    NSURL *coordinatorURL = [appDocumentsDir URLByAppendingPathComponent:coordinatorPath];
    
    
    // Request automatic lightweight migration using the options dictionary
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                             [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
    
    NSError *error = nil;
    _persistentCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.managedObjectModel];
    if(![_persistentCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:coordinatorURL options:options error:&error]) {
        
        NSLog(@"DACoreData Core Data unresolved error %@, %@", error, [error userInfo]);
        
#ifdef _DEBUG_
        abort();
#else
        [[self class] throwCoordinatorCreateException];
#endif
    }
    
    // Encrypt the file stored on disk
    NSDictionary *fileAttributes = [NSDictionary dictionaryWithObject:NSFileProtectionComplete forKey:NSFileProtectionKey];
    if(![[NSFileManager defaultManager] setAttributes:fileAttributes ofItemAtPath:[coordinatorURL path] error:&error]) {
        NSLog(@"DACoreData store encryption error %@, %@", error, [error userInfo]);
    }
    
    return _persistentCoordinator;

}










#pragma mark - Context notifications

/**
 * Starts observing the specified context
 *
 * @param NSManagedObjectContext*
 * @return void
 * @access public
 */
- (void)observeContext:(NSManagedObjectContext *)otherContext {
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(mergeChangesFromNotification:)
												 name:NSManagedObjectContextDidSaveNotification
											   object:otherContext];
}



/**
 * Stops observing the specified context
 *
 * @param NSManagedObjectContext*
 * @return void
 * @access public
 */
- (void)stopObservingContext:(NSManagedObjectContext *)otherContext {
	[[NSNotificationCenter defaultCenter] removeObserver:self
													name:NSManagedObjectContextDidSaveNotification
												  object:otherContext];
}



/**
 * Merges changes into the main context on the main thread
 *
 * @param NSNotification*
 * @return void
 * @access public
 */
- (void)mergeChangesFromNotification:(NSNotification *)notification {
    
    // Only on main thread
    if(![NSThread isMainThread]) {
		[self performSelectorOnMainThread:@selector(mergeChangesFromNotification:) withObject:notification waitUntilDone:NO];
        return;
    }
    
    NSManagedObjectContext *mainContext = [DACoreData defaultContext];
    
    [mainContext mergeChangesFromContextDidSaveNotification:notification];
}



@end
