//
//  DACoreData.h
//  DACoreData_Test
//
//  Created by David Ansermot on 09.12.13.
//  Copyright (c) 2013 mArmApp. All rights reserved.
//
//  Version 1.0.1
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#define kNotificationDACoreDataHasSaved @"DACoreData_Context_Saved_Notification"

/**
 * Main CoreData Class
 *
 * Manage the main stack
 */
@interface DACoreData : NSObject {
    @protected
    NSManagedObjectContext *_managedContext;
    NSManagedObjectModel *_managedModel;
    NSPersistentStoreCoordinator *_persistentCoordinator;
}

//
// Public properties
//
@property (strong, nonatomic, readonly) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic, readonly) NSManagedObjectModel *managedObjectModel;
@property (strong, nonatomic, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;


//
// Return the singleton instance of DACoreData
//
+ (DACoreData *)sharedCoreData;



//
// Managed context related methods
//
+ (NSManagedObjectContext *)defaultContext;
+ (BOOL)saveDefaultContext;
+ (void)haveToSaveDefaultContext;


//
// Misc method
//
- (NSString *)applicationDocumentsDirectory;
+ (void)setModelName:(NSString *)modelName;


//
// Tasks related methods
//
+ (void)runTaskBlock:(void(^)(NSManagedObjectContext *context))block;
+ (void)runTaskBlockInBackground:(void (^)(NSManagedObjectContext *context))block;
+ (void)runTaskBlockInBackground:(void (^)(NSManagedObjectContext *context))block completion:(void (^)(void))callback;
+ (void)runTaskBlockInBackground:(void (^)(NSManagedObjectContext *context))block completion:(void (^)(void))callback priority:(NSInteger)priority;

@end
