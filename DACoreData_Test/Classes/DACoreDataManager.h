//
//  DACoreDataManager.h
//  DACoreData_Test
//
//  Created by David Ansermot on 10.12.13.
//  Copyright (c) 2013 mArmApp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DACoreData.h"

/**
 * CoreData Wrapper
 *
 * Manage the DACoreData object and provide usefull methods
 */
@interface DACoreDataManager : NSObject

// Flag for autosaving in some methods like deleting
@property (assign, nonatomic) BOOL autosaveEnabled;

// Default managed object context
@property (strong, nonatomic, readonly) NSManagedObjectContext *defaultContext;


//
// Return the singleton instance of DACoreData
//
+ (DACoreDataManager *)sharedManager;


//
// General methods
//
- (BOOL)save;
+ (void)setModelName:(NSString *)modelName;
+ (NSManagedObjectContext *)defaultContext;


//
// Selection methods
//
- (NSArray *)selectAllEntitiesWithName:(NSString *)entityName error:(NSError **)error;
- (NSArray *)selectAllEntitiesWithName:(NSString *)entityName withPredicate:(NSPredicate *)predicate error:(NSError **)error;
- (NSArray *)selectAllEntitiesWithName:(NSString *)entityName withSortDescriptor:(NSSortDescriptor *)sortDescriptor error:(NSError **)error;
- (NSArray *)selectAllEntitiesWithName:(NSString *)entityName withSortDescriptor:(NSSortDescriptor *)sortDescriptor andPredicate:(NSPredicate *)predicate error:(NSError **)error;
- (NSArray *)selectAllEntitiesWithName:(NSString *)entityName withSortDescriptorArray:(NSArray *)sortDescriptorArray andPredicate:(NSPredicate *)predicate error:(NSError **)error;


//
// Creation methods
//
- (NSManagedObject *)createNewEntityWithName:(NSString *)entityName;
- (NSFetchedResultsController *)createNewFetchedResultsControllerForEntity:(NSString *)entityName descriptors:(NSArray *)sortDescriptorArrayOrNil predicate:(NSPredicate *)aPredicateOrNil;
- (NSFetchedResultsController *)createNewFetchedResultsControllerForEntity:(NSString *)entityName sortedOnKey:(NSString *)sortKey;
- (NSFetchedResultsController *)createNewFetchedResultsControllerForEntity:(NSString *)entityName sortedOnKey:(NSString *)sortKey ascending:(BOOL)isAscending;
- (NSFetchedResultsController *)createNewFetchedResultsControllerForEntity:(NSString *)entityName sortedOnKey:(NSString *)sortKey ascending:(BOOL)isAscending sectionNameKey:(NSString *)sectionNameKey;
- (NSFetchedResultsController *)createNewFetchedResultsControllerForEntity:(NSString *)entityName sortedOnKey:(NSString *)sortKey ascending:(BOOL)isAscending sectionNameKey:(NSString *)sectionNameKey cacheName:(NSString *)cacheName;


//
// Deleting methods
//
- (BOOL)deleteAllEntitiesWithName:(NSString *)entityName;
- (BOOL)deleteEntityObject:(NSManagedObject *)object;
- (NSSet *)deletedObjects;

@end
