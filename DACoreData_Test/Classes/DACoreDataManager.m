//
//  DACoreDataManager.m
//  DACoreData_Test
//
//  Created by David Ansermot on 10.12.13.
//  Copyright (c) 2013 mArmApp. All rights reserved.
//

#import "DACoreDataManager.h"

#pragma mark - Class implementation

@implementation DACoreDataManager

// Handle the singleton instance of the class
static DACoreDataManager __strong *__instance = nil;











#pragma mark - Class initialization

/**
 * Default init method
 *
 * @param void
 * @return void
 * @access public
 */
- (id)init {
    if (self = [super init]) { }
    return self;
}










#pragma mark - Singleton design pattern methods

/**
 * Return the singleton object of the class
 *
 * @param void
 * @return DACoreDataManager*
 * @access public
 * @static
 */
+ (DACoreDataManager *)sharedManager {
    static dispatch_once_t once;
    
    dispatch_once(&once, ^{
        __instance = [[DACoreDataManager alloc] init];
        __instance.autosaveEnabled = YES;
    });
    return __instance;
}











#pragma mark - General  methods

/**
 * Saves the context
 *
 * @param void
 * @return BOOL
 * @access public
 */
- (BOOL)save {
    return [DACoreData saveDefaultContext];
}



/**
 * Set the Data Model filename
 *
 * @param NSString*
 * @return void
 * @access public
 * @static
 */
+ (void)setModelName:(NSString *)modelName {
    [DACoreData setModelName:modelName];
}














#pragma mark - Selection  methods

/**
 * Return an array of all entity objects from the coredata
 *
 * @param NSString*
 * @return NSArray
 * @access public
 */
- (NSArray *)selectAllEntitiesWithName:(NSString *)entityName error:(NSError **)error {
    return [self selectAllEntitiesWithName:entityName
                        withSortDescriptor:nil
                              andPredicate:nil
                                     error:error];
}


/**
 * Return an array of all entity objects from the coredata
 *
 * @param NSString*
 * @return NSArray
 * @access public
 */
- (NSArray *)selectAllEntitiesWithName:(NSString *)entityName withPredicate:(NSPredicate *)predicate error:(NSError **)error {
    
    return [self selectAllEntitiesWithName:entityName
                        withSortDescriptor:nil
                              andPredicate:predicate
                                     error:error];
}


/**
 * Return an array of all entity objects from the coredata
 *
 * @param NSString*
 * @return NSArray
 * @access public
 */
- (NSArray *)selectAllEntitiesWithName:(NSString *)entityName withSortDescriptor:(NSSortDescriptor *)sortDescriptor error:(NSError **)error {
    return [self selectAllEntitiesWithName:entityName
                        withSortDescriptor:sortDescriptor
                              andPredicate:nil
                                     error:error];
}


/**
 * Return an array of all entity objects from the coredata
 *
 * @param NSString*
 * @return NSArray
 * @access public
 */
- (NSArray *)selectAllEntitiesWithName:(NSString *)entityName withSortDescriptor:(NSSortDescriptor *)sortDescriptor andPredicate:(NSPredicate *)predicate error:(NSError **)error {
    
    if (sortDescriptor == nil) {
        return [self selectAllEntitiesWithName:entityName
                       withSortDescriptorArray:nil
                                  andPredicate:predicate
                                         error:error];
    } else {
    
        return [self selectAllEntitiesWithName:entityName
                       withSortDescriptorArray:@[sortDescriptor]
                                  andPredicate:predicate
                                         error:error];
    }
}


/**
 * Return an array of all entity objects from the coredata
 *
 * @param NSString*
 * @return NSArray
 * @access public
 */
- (NSArray *)selectAllEntitiesWithName:(NSString *)entityName withSortDescriptorArray:(NSArray *)sortDescriptorArray andPredicate:(NSPredicate *)predicate error:(NSError **)error {
    
    // Create description
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:entityName inManagedObjectContext:[DACoreData sharedCoreData].managedObjectContext];
    
    // Create request
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDescription];
    [request setPredicate:predicate];
    [request setSortDescriptors:sortDescriptorArray];
    
    // Festch request and return result
    NSArray *array = [[DACoreData sharedCoreData].managedObjectContext executeFetchRequest:request error:error];
    return array;
}












#pragma mark - Selection  methods

/**
 * Create a new entity
 *
 * @param NSString*
 * @return NSManagedObject*
 * @access public
 * @todo Try to cast the returned object with the entityName as class
 */
- (NSManagedObject *)createNewEntityWithName:(NSString *)entityName {
#ifdef _DEBUG_
    NSLog(@"Create object of name '%@'", entityName);
#endif
    return [NSEntityDescription insertNewObjectForEntityForName:entityName inManagedObjectContext:[DACoreData sharedCoreData].managedObjectContext];
}



/**
 * Create a NSFetchedResultsController with descriptors array and a predicate
 *
 * @param NSString*
 * @return NSFetchedResultsController*
 */
- (NSFetchedResultsController *)createNewFetchedResultsControllerForEntity:(NSString *)entityName descriptors:(NSArray *)sortDescriptorArrayOrNil predicate:(NSPredicate *)aPredicateOrNil {
    
    // Create the request
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:entityName inManagedObjectContext:[DACoreData sharedCoreData].managedObjectContext];
    [request setEntity:entityDescription];
    
    // Sets the sort descriptors
    if (sortDescriptorArrayOrNil != nil) {
        [request setSortDescriptors:sortDescriptorArrayOrNil];
    }
    
    // Sets the predicate
    if (aPredicateOrNil != nil) {
        [request setPredicate:aPredicateOrNil];
    }
        
    // Creates the controller and return
    return [[NSFetchedResultsController alloc] initWithFetchRequest:request
                                               managedObjectContext:[DACoreData sharedCoreData].managedObjectContext
                                                 sectionNameKeyPath:nil
                                                          cacheName:nil];

}



/**
 * Create a NSFetchedResultsController with all the parameters
 *
 * @param NSString*
 * @return NSFetchedResultsController*
 */
- (NSFetchedResultsController *)createNewFetchedResultsControllerForEntity:(NSString *)entityName sortedOnKey:(NSString *)sortKey {
    return [self createNewFetchedResultsControllerForEntity:entityName sortedOnKey:sortKey ascending:YES sectionNameKey:nil cacheName:nil];
}



/**
 * Create a NSFetchedResultsController with all the parameters
 *
 * @param NSString*
 * @param BOOL
 * @return NSFetchedResultsController*
 */
- (NSFetchedResultsController *)createNewFetchedResultsControllerForEntity:(NSString *)entityName sortedOnKey:(NSString *)sortKey ascending:(BOOL)isAscending {
    return [self createNewFetchedResultsControllerForEntity:entityName sortedOnKey:sortKey ascending:isAscending sectionNameKey:nil cacheName:nil];
}



/**
 * Create a NSFetchedResultsController with all the parameters
 *
 * @param NSString*
 * @param BOOL
 * @param NSString*
 * @return NSFetchedResultsController*
 */
- (NSFetchedResultsController *)createNewFetchedResultsControllerForEntity:(NSString *)entityName sortedOnKey:(NSString *)sortKey ascending:(BOOL)isAscending sectionNameKey:(NSString *)sectionNameKey {
    return [self createNewFetchedResultsControllerForEntity:entityName sortedOnKey:sortKey ascending:isAscending sectionNameKey:sectionNameKey cacheName:nil];
}




/**
 * Create a NSFetchedResultsController with all the parameters
 *
 * @param NSString*
 * @param BOOL
 * @param NSString*
 * @param NSString*
 * @return NSFetchedResultsController*
 */
- (NSFetchedResultsController *)createNewFetchedResultsControllerForEntity:(NSString *)entityName sortedOnKey:(NSString *)sortKey ascending:(BOOL)isAscending sectionNameKey:(NSString *)sectionNameKey cacheName:(NSString *)cacheName {
    
    // Create the request
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:entityName inManagedObjectContext:[DACoreData sharedCoreData].managedObjectContext];
    [request setEntity:entityDescription];
    
    // Sets the sort descriptor
    NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:sortKey ascending:isAscending];
    NSArray *descriptorsArray = [[NSArray alloc] initWithObjects:descriptor, nil];
    [request setSortDescriptors:descriptorsArray];
    
    // Creates the controller and return
    return [[NSFetchedResultsController alloc] initWithFetchRequest:request
                                               managedObjectContext:[DACoreData sharedCoreData].managedObjectContext
                                                 sectionNameKeyPath:sectionNameKey
                                                          cacheName:cacheName];
}












#pragma mark - Deleting  methods

/**
 * Delete all entities of a sort [autosaving]
 *
 * @param NSString*
 * @return BOOL
 * @access public
 */
- (BOOL)deleteAllEntitiesWithName:(NSString *)entityName {
    
    NSError *error = nil;
    NSArray *objects = [self selectAllEntitiesWithName:entityName error:&error];
    if (error != nil) {
        return NO;
    }
    
    for (NSManagedObject *object in objects) {
        [[DACoreData sharedCoreData].managedObjectContext deleteObject:object];
    }
#ifdef _DEBUG_
    NSLog(@"All objects deleted for entity '%@'", entityName);
#endif
    
    // Autosaving
    if (self.autosaveEnabled) {
        return [DACoreData saveDefaultContext];
    }
    return YES;
}



/**
 * Delete an object [autosaving]
 *
 * @param NSString*
 * @return BOOL
 * @access public
 */
- (BOOL)deleteEntityObject:(id)object {
    
    [[DACoreData sharedCoreData].managedObjectContext deleteObject:object];
#ifdef _DEBUG_
    NSLog(@"Object deleted : %@", object);
#endif
    
    // Autosaving
    if (self.autosaveEnabled) {
        return [DACoreData saveDefaultContext];
    }
    return YES;
}



/**
 * Delete all entities of a sort
 *
 * @param NSString*
 * @return BOOL
 * @access public
 */
- (NSSet *)deletedObjects {
    return [[DACoreData sharedCoreData].managedObjectContext deletedObjects];
}

@end
