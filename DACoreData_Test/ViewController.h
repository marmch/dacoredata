//
//  ViewController.h
//  DACoreData_Test
//
//  Created by David Ansermot on 09.12.13.
//  Copyright (c) 2013 mArmApp. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Guy;
@class Thinking;
@class NameViewController;

@interface ViewController : UIViewController <NameViewControllerDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property (weak, nonatomic) IBOutlet UITextField *thinkTextfield;

@property (strong, nonatomic) Guy *currentGuy;
@property (strong, nonatomic) NSArray *thinkings;

- (IBAction)thinkButton_clicked:(UIButton *)sender;
- (IBAction)flushButton_clicked:(UIButton *)sender;

@end
