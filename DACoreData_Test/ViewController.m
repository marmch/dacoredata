//
//  ViewController.m
//  DACoreData_Test
//
//  Created by David Ansermot on 09.12.13.
//  Copyright (c) 2013 mArmApp. All rights reserved.
//

#import "ViewController.h"

@class DACoreDataManager;

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(coreDataSaved:) name:kNotificationDACoreDataHasSaved object:nil];
    self.thinkTextfield.delegate = self;
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    NSError *error = nil;
    NSArray *array = [[DACoreDataManager sharedManager] selectAllEntitiesWithName:@"Guy" error:&error];
    
    if (array == nil) { NSLog(@"fetch error"); }
    else {
        if ([array count] > 0) {
            self.currentGuy = (Guy *)[array firstObject];
            self.nameLabel.text = [NSString stringWithFormat:@"Welcome %@ %@", self.currentGuy.firstname, self.currentGuy.lastname];
        } else {
            self.currentGuy = nil;
        }
    }
    error = nil;
    
    
    NSArray *thinkings = [[DACoreDataManager sharedManager] selectAllEntitiesWithName:@"Thinking" error:&error];
    if (thinkings == nil) { NSLog(@"fetch error"); }
    else {
        self.thinkings = thinkings;
        if ([thinkings count] == 0) {
            self.countLabel.text = @"You have not thought yet ;P";
        } else {
            self.countLabel.text = [NSString stringWithFormat:@"You have %d memories", [thinkings count]];
        }
    }

}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
   
    if (self.currentGuy == nil) {
        [self performSegueWithIdentifier:@"nameSegue" sender:nil];
    }
}

- (void)didSavedFirstname:(NSString *)firstname andLastname:(NSString *)lastname {
    
    Guy *guy = (Guy *)[[DACoreDataManager sharedManager] createNewEntityWithName:@"Guy"];
    guy.firstname = firstname;
    guy.lastname = lastname;
    
    [DACoreData saveDefaultContext];
    self.currentGuy = guy;
    
    self.nameLabel.text = [NSString stringWithFormat:@"Welcome %@ %@", firstname, lastname];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"nameSegue"]) {
        NameViewController *controller = (NameViewController *)segue.destinationViewController;
        controller.delegate = self;
    }
}

- (IBAction)thinkButton_clicked:(UIButton *)sender {
    
    if (![self.thinkTextfield.text isEqualToString:@""]) {
        
        [self.thinkTextfield resignFirstResponder];
        
        Thinking *thinking = (Thinking *)[[DACoreDataManager sharedManager] createNewEntityWithName:@"Thinking"];
        thinking.title = self.thinkTextfield.text;
        thinking.content = [NSString stringWithFormat:@"Thinking number %d", [self.thinkings count]];
        [[DACoreDataManager sharedManager] save];
        
        self.thinkTextfield.text = @"";
        
        NSError *error = nil;
        self.thinkings = [[DACoreDataManager sharedManager] selectAllEntitiesWithName:@"Thinking" error:&error];
        [self.countLabel setText:[NSString stringWithFormat:@"You have %d memories", [self.thinkings count]]];
    }
    
}


- (IBAction)flushButton_clicked:(UIButton *)sender {
    [[DACoreDataManager sharedManager] deleteAllEntitiesWithName:@"Thinking"];
    
    NSError *error = nil;
    self.thinkings = [[DACoreDataManager sharedManager] selectAllEntitiesWithName:@"Thinking" error:&error];
    [self.countLabel setText:[NSString stringWithFormat:@"You have %d memories", [self.thinkings count]]];
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
