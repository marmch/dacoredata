//
//  AppDelegate.h
//  DACoreData_Test
//
//  Created by David Ansermot on 09.12.13.
//  Copyright (c) 2013 mArmApp. All rights reserved.
//

#import <UIKit/UIKit.h>

#define _DEBUG_ BOOL

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
