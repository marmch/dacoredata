//
//  NameViewController.h
//  DACoreData_Test
//
//  Created by David Ansermot on 10.12.13.
//  Copyright (c) 2013 mArmApp. All rights reserved.
//

#import <UIKit/UIKit.h>



@protocol NameViewControllerDelegate <NSObject>

@required
- (void)didSavedFirstname:(NSString *)firstname andLastname:(NSString *)lastname;

@end

@interface NameViewController : UIViewController

@property (strong, nonatomic) id <NameViewControllerDelegate> delegate;

@property (weak, nonatomic) IBOutlet UITextField *firstnameTF;
@property (weak, nonatomic) IBOutlet UITextField *lastnameTF;

- (IBAction)doneButton_clicked:(UIBarButtonItem *)sender;

@end
